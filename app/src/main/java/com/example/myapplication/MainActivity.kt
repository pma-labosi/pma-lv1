package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    lateinit var button : Button
    lateinit var tekstTv : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById(R.id.button)
        tekstTv = findViewById(R.id.textView)

        button.setOnClickListener {
            if (tekstTv.getText().equals("Hello World!")) {
                tekstTv.setText("Dobro jutro!")
            }
            else if (tekstTv.getText().equals("Dobro jutro!")) {
                tekstTv.setText("Dobar dan!")
            }
            else if (tekstTv.getText().equals("Dobar dan!")) {
                tekstTv.setText("Dobra večer!")
            }
            else {
                tekstTv.setText("Dobro jutro!")
            }
        }
    }
}